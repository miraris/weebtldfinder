FROM node:alpine

RUN apk add --update git tor torsocks && \
	rm -rf /tmp/* /var/cache/apk/*

WORKDIR /usr/src/app

COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile --no-cache --production
COPY . .

CMD [ "./app" ]
