const got = require("got");
const { JSDOM } = require("jsdom");
// const { promises: dnsPromises } = require("dns");
const whois = require("whois-json");
const { promises: fsPromises } = require("fs");

// "https://anidb.net/perl-bin/animedb.pl?show=characterlist&orderby.waifu=0.2&orderby.trash=0&orderby.rating=0&orderby.name=1.1&orderby.gender=0.2&orderby.chartype=0&orderby.bloodtype=0&orderby.birthdate=0&orderby.age=0&orderbar=1&noalias=1&page=1&view=list";

const loadCharacters = async (limit = 20) => {
  let load = true;
  let page = 0;
  let names = [];

  while (load) {
    const {
      window: { document }
    } = await JSDOM.fromURL(
      `https://anidb.net/perl-bin/animedb.pl?show=characterlist&orderby.waifu=0.2&orderby.trash=0&orderby.rating=0&orderby.name=1.1&orderby.gender=0.2&orderby.chartype=0&orderby.bloodtype=0&orderby.birthdate=0&orderby.age=0&orderbar=1&noalias=1&page=${page}&view=list`
    );

    names = names.concat(
      [...document.querySelectorAll('td[data-label="Title"] > a')].map(
        a => a.textContent
      )
    );

    // break;
    page++;
    if (page === limit) break;
  }

  return names;
};

const saveCharacters = arr =>
  fsPromises.writeFile("characters.json", JSON.stringify(arr), { flag: "wx" });

const loadTlds = () =>
  got(
    "https://raw.githubusercontent.com/umpirsky/tld-list/master/data/en/tld.json",
    { json: true }
  );

const findTld = async (tlds, name, opts) => {
  let words = name.split(" ");
  const available = [];

  // other variants
//   if (words.length > 1) {
//     words = words.concat(words.join("-"), words.join(""));
//   }

  for (const word of words) {
    const { length } = word;
    for (const tld of tlds) {
      if (length <= tld.length || !word.endsWith(tld)) continue;
      const dn = `${word.substring(0, length - tld.length)}.${tld}`;

      console.log(`trying tld: ${dn}`);

      //   const { domainName, registrar } = await whois(dn);
      //   if (domainName && registrar) continue;

      const { body: res } = await got(
        `https://njal.la/list/?search=${dn}&format=json`,
        {
          json: true,
          ...opts
        }
      );

      if (res.length > 2 || res.length === 0 || res[0].status !== "available")
        continue;

      available.push({ dn, word, status: res[0].status, price: res[0].price });
    }
  }

  return available;
};

const appendDomains = async domains => {
  let file = await fsPromises.readFile("domains.json").then(JSON.parse);
  file = file.concat(domains);
  await fsPromises.writeFile("domains.json", JSON.stringify(file));
};

const lastDomain = () =>
  fsPromises
    .readFile("domains.json")
    .then(JSON.parse)
    .then(arr => arr[arr.length - 1].dn);

module.exports = {
  loadCharacters,
  saveCharacters,
  loadTlds,
  findTld,
  appendDomains,
  lastDomain
};
