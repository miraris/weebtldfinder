const chalk = require("chalk");
const log = console.log;
const {
  findTld,
  loadCharacters,
  loadTlds,
  appendDomains
} = require("./lib/util");
const SocksProxyAgent = require("socks-proxy-agent");

const run = async (_, { tor }) => {
  log(chalk.green("Starting.\n"));

  const chars = await loadCharacters();
  const { body: tlds } = await loadTlds();
  const agent = tor && new SocksProxyAgent("socks://127.0.0.1:9050", true);

  for (const char of chars) {
    const av = await findTld(Object.values(tlds), char, { agent });
    if (av.length) {
      console.log(av);
      appendDomains(av);
    }
  }
};

module.exports = run;
