const { loadCharacters, loadTlds } = require("../lib/util");
const whois = require("whois-json");

describe("utils", () => {
  it("should load characters", async () => {
    const chars = await loadCharacters(1);

    // console.log(chars);

    expect(chars).toBeTruthy();
  });

  it("should load tlds", async () => {
    const tlds = await loadTlds();

    // console.log(tlds.body);

    expect(tlds).toBeTruthy();
  });

  it("gets whois", async () => {
    const res = await whois("miraris.moe");

    // console.log(res);
    expect(res).toBeTruthy();
  });
});
