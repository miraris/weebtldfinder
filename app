#!/usr/bin/env node
"use strict";
const meow = require("meow");
const foo = require(".");

const cli = meow(
  `
	Usage
	  $ foo <input>

	Options
	  --tor		Use tor for all requests

	Examples
	  $ app --tor
`,
  {
    flags: {
      tor: {
        type: "boolean"
      }
    }
  }
);
/*
{
	input: ['unicorns'],
	flags: {rainbow: true},
	...
}
*/

foo(cli.input[0], cli.flags);
